const normalizr = require('normalizr')
const { normalize, denormalize, schema } = normalizr
const util = require('util')
const originalData = require('./originalData')

// función para ver los datos de manera linda en la consola
const print = (obj) => {
    console.log(util.inspect(obj, false, 12, true))
}


// Definimos los esquemas

const userSchema = new schema.Entity('users') 

const commentSchema = new schema.Entity('comments', {
    commenter: userSchema
})

const articleSchema = new schema.Entity('articles',{
    author: userSchema,
    comments: [ commentSchema ]
})

const postSchema = new schema.Entity('posts', {
    posts: [ articleSchema ]
})

// datos originales
console.log('-----------------------Datos originales-----------------------')
// print(originalData)

console.log(JSON.stringify(originalData).length)

console.log('-----------------------Datos normalizado-----------------------')
const normalizedData = normalize(originalData, postSchema)
// print(normalizedData)
console.log(JSON.stringify(normalizedData).length)
// console.log

console.log('-----------------------Datos desnormalizado-----------------------')
const denormalizedData = denormalize( normalizedData, postSchema, normalizedData.entities)
print(JSON.stringify(normalizedData))

































