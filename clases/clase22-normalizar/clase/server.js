const normalizr = require('normalizr');
const { normalize, denormalize, schema } = normalizr
const originalData = require('./originalData') 
const util = require('util')



// Esquema para los usuarios (autores y comentadores)
const userSchema = new schema.Entity('users')

// Esquema para los comentadores
const commentSchema = new schema.Entity('comments', {
    commenter: userSchema
})

// Definimos un esquema para los artículos
const articleSchema = new schema.Entity('articles', {
    autor: userSchema,
    comments: [commentSchema]
})

// Definimos un esquema para los post (array de articulos )
const postSchema = new schema.Entity('posts', {
    posts: [ articleSchema ]
})


const print = (obj) => {
    console.log(util.inspect(obj, false, 12, true /* enable colors */))
}

console.log('------------- Objeto Original ---------------')
print(JSON.stringify(originalData).length)


console.log('------------- Objeto Normalizado ---------------')
const normalizedData = normalize(originalData, postSchema)
print(JSON.stringify(normalizedData).length)

console.log('------------- Objeto Denormalizado ---------------')
const denormalizedData = denormalize(normalizedData, postSchema, normalizedData.entities)
print(JSON.stringify(denormalizedData).length)
// console.log(JSON.stringify(denormalizedData).length)
// console.log(denormalizedData)

