const express = require('express')
const { Router } = express
const cookieParser = require('cookie-parser')

const app = express()
const routerUsuarios = Router()
// const routerProductos = Router()

app.use(express.json())
app.use('/static', express.static(__dirname + 'public'))
app.use(express.static('file'))
app.use(cookieParser())

app.use( (req, res, next) => {
    console.log('Time:', Date.now());
    next()
})


const funcMidUno = (req, res, next) => {
    req.mensaje1 = 'Hola'
    next()
}

const funcMidDos = (req, res, next) => {
    req.mensaje2 = 'mundo'
    next()
}
  



const arrayPersonas =[]
//////////// usuarios //////////////

// 

routerUsuarios.use((req, res, next) => {
    console.log('Time:', Date.now());
    next();
  });
  

routerUsuarios.get('/', funcMidUno, funcMidDos ,(req, res)=>{   
    const {mensaje1, mensaje2} = req
    console.log(mensaje1)
    res.json({
        ok: true,
        mensaje: mensaje1 + ' ' + mensaje2,
        personas: arrayPersonas
    })
    // res.sendFile( __dirname + '/public/indexForm.html' )
})

routerUsuarios.post('/', (req, res)=>{
    const {nombre, email} = req.body

    res.json({
        ok: true,
        mensaje: 'Todo bien el post',
        persona: [...arrayPersonas, {nombre, email}]
    })
})


////////////////////////////////



app.use('/api/personas', routerUsuarios)
// app.use('/api/prodcutos', routerProductos)

app.use( (err, req, res, next) => {
    console.error(err.stack);
    res.send({err});
  })
  

const PORT = process.env.PORT || 4000
app.listen(PORT, ()=>{
    console.log('server is running on port 4000')
})






