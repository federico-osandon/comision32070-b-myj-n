const express = require('express')
const { Router } = express
var cookieParser = require('cookie-parser')

const app = express()
const routerUsuarios = Router()

// load the cookie-parsing middleware
app.use(cookieParser())

// app.use(express.static('public'))
app.use('/statics',express.static(__dirname + '/public'))



app.use(function (req, res, next) {
  console.log('Time:', Date.now());
  next();
})

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send('Something broke!');
  })

routerUsuarios.use(function (req, res, next) {
    console.log('Time:', Date.now())
    next();
})
  


/////////////////////////////// USUARIOS ///////////////////////////////

routerUsuarios.get('/', (req, res) => {
    console.log(__dirname)
    res.send('<h1>Usuarios</h1>' )
})

routerUsuarios.post('/', (req, res) => {
    res.json({
        msg: 'post realizado '
    })
})

///////////////////////// productos ///////////////////////////////
const routerProductos = Router()

routerProductos.get( '/', (req, res) => {
    res.json({
        msg: 'get Productos realizado'        
    })
})


app.use('/api/usuarios', routerUsuarios)
app.use('/api/productos', routerProductos)


const PORT = process.env.PORT || 4000

const server = app.listen(PORT, () => {
    console.log(`Server running in ${server.address().port}`)
})


  