// console.log(process.argv)
// // console.log(process.argv[1])

// const array = process.argv

// array.forEach((item, index) => {
//     console.log(`${index} --> ${item}`)
// })

/// ___________________________ minimist _____________________________________///


/// minimist module

// const parseArgs = require('minimist')

// const args = parseArgs(process.argv.slice(2))


// console.log(args)

// console.log(parseArgs(['1', '2', '3', '4']));
// { _: [ 1, 2, 3, 4 ] }

// console.log(parseArgs(['-a', '1', '-b', '2', '3', '4']));
// // { _: [ 3, 4 ], a: 1, b: 2 }

// console.log(parseArgs(['--n1', '1123', '--n2', '2', '3', '4']));
// // { _: [ 3, 4 ], n1: 1, n2: 2 }

// console.log(parseArgs(['-ala', '1', '-b', '2', '--colores', '--watch']));
// console.log(parseArgs(['-fede']));
// // { _: [], a: 1, b: 2, colores: true, cursiva: true }

// console.log(parseArgs(['-a', '1', '-b', '2', '-c', '-x']));
// // { _: [], a: 1, b: 2, c: true, x: true }



///// mas ejemplos
// const options = { default: { nombre: 'pepe', apellido: 'copado' }}

// console.log(parseArgs(['-a', '1', '-b', '2', 'un-valor-suelto', '--nombre', 'juanita'], options));
// { _: [ 'un-valor-suelto' ], a: 1, b: 2, nombre: 'juanita', apellido: 'copado' }


// const options = { alias: { a: 'campoA', b: 'campoB', } }

// console.log(parseArgs(['-a', '1', '-b', '2'], options))
// { _: [], a: 1, campoA: 1, b: 2, campoB: 2 }


// _______________________________________________ module yargs___________________________________//

// const yargs = require('yargs/yargs')(process.argv.slice(2))
// const args = yargs.argv

// console.log(args)

// const args = yargs.default(
//     {
//         nombre: 'pepe',
//         apellido: 'copado'
//     }
// ).argv

// console.log(args)
// -a 1 -b 2 un-valor-suelto --nombre juanita

// const args = yargs.alias(
// {
//         n: 'nombre',
//         a: 'apellido'
//     }
// ).argv
// console.log(args)
// node server -a 1 -n 2


// const args = yargs
//   .boolean('vivo')
//   .argv
        
// console.log(args)

// _________________________________ server node ______________________________///
// server node 
// require('dotenv').config()
// const config = require('./config')
// const express= require('express')
// const configEnv = require('./configEnv')

// const app = express()



// console.log(config.NODE_ENV)
// console.log(process.env.NODE_ENV)
// console.log(process.env.FRENTE)
// console.log(process.env.FONDO)
// console.log(process.env.PORT)
// console.log(process.env.HOST)

// app.get('/', (req, res) => {
//     res.send('Hola mundo')
// })

// app.listen(config.PORT, process.env.HOST, () => {
//     console.log(`Escuchando en el puerto ${config.PORT}`)
// })