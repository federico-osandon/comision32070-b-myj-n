// console.log(process.argv)

// const arrayProcess = process.argv

// arrayProcess.forEach((element, i) => {
//     console.log(`${i} -> ${element}`)
//     // console.log(element)
// });


const parseArgs = require('minimist')

// const args = parseArgs(process.argv.slice())
// const args1 = parseArgs(process.argv.slice(2))


// ejemplos de uso
// const args = parseArgs(['1', '2', '3', '4']) 
// console.log(args);
// { _: [ 1, 2, 3, 4 ] }

// const args = parseArgs(['-a', '1', '-b', '2', '3', '4']) 
// console.log(args);
// { _: [ 3, 4 ], a: 1, b: 2 }

// const args = parseArgs(['--n1', '1', '--n2', '2', '3', '4']) 
// console.log(args);
// { _: [ 3, 4 ], n1: 1, n2: 2 }

// const args = parseArgs(['-a', '1', '-b', '2', '--colores', '--cursiva']) 
// console.log(args);
// { _: [], a: 1, b: 2, colores: true, cursiva: true }

// const args = parseArgs(['-a', '1', '-b', '2', '-c', '-x']) 
// console.log(args);
// { _: [], a: 1, b: 2, c: true, x: true }


// console.log(args1)

// const options = { default: { nombre: 'pepe', apellido: 'copado' }}

// console.log(parseArgs(['-a', '1', '-b', '2', 'un-valor-suelto', '--nombre', 'juanita'], options));
// { _: [ 'un-valor-suelto' ], a: 1, b: 2, nombre: 'juanita', apellido: 'copado' }


const options2 = { alias: { a: 'campoA', b: 'campoB', } }

// console.log(parseArgs(['-a', '1', '-b', '2'], options2));
console.log(parseArgs(process.argv.slice(2), options2));
console.log(process.argv.slice(2))
// { _: [], a: 1, campoA: 1, b: 2, campoB: 2 }

