const express = require('express')
const config = require('./config')

const app = express()

console.log(config.NODE_ENV)

app.get('/', (req, res) => {
    res.send('Hello World!')
})

app.listen(config.PORT, config.HOST,() => {
    console.log(`Example app listening at http://localhost:${config.PORT}`)
})
