
//_____________________________ gmail __________________________________________
import { createTransport } from 'nodemailer';

// nombre  del mail del que envia
const TEST_MAIL = 'projectodigitalgen@gmail.com'

const transporter = createTransport({
   service: 'gmail',
   port: 587,
   auth: {
       user: TEST_MAIL,
       pass: 'rvtuloohntujtdnp'
   }
})

const mailOptions = {
    from: 'Servidor Node.js',
    to: 'projectodigitalgen@gmail.com',
    subject: 'Mail de prueba desde Node.js',
    html: '<h1 style="color: blue;">Contenido de prueba desde <span style="color: green;">Node.js con Fede</span></h1>',
    attachments: [
        {
            path: 'https://raw.githubusercontent.com/andris9/Nodemailer/master/assets/nm_logo_200x136.png'
        }
    ]
}

;(async () => {
    try {
        const info = await transporter.sendMail(mailOptions)
        console.log(info)
     } catch (error) {
        console.log(error)
     }
})()

// rvtuloohntujtdnp