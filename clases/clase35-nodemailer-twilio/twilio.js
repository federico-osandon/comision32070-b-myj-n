// import twilio from 'twilio'
import  twilio from 'twilio'
import dotenv from 'dotenv'

dotenv.config()

const accountSid = process.env.ACCOUNT_SID
const authToken = process.env.AUTH_TOKEN

const client = twilio(accountSid, authToken)

;( async () =>{
    try {
        const message = await client.messages.create({
            body: 'Hola soy un SMS desde Node.js!',
            from: process.env.PHONE_ENVIA,
            to: process.env.PHONE_RECIBE
        })
        console.log(message)
    } catch (error) {
        console.log(error)
    }
})()

// detalles de la cuenta
// no hay que cambiar el país de facturación 
// solo se puede enviar mensajes con la cuenta sandbox al número de teléfono que se ha registrado en la cuenta sandbox


// logueado con defe014@gmail.com
//cuentas de prueba

// 