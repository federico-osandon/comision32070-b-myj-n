const express = require('express')


const app = express()
let counter = 0
app.get('/', (req,res)=>{
    res.send('<h1>Hola mundo</h1>')
})
app.get('/visitas', (req,res)=>{
    counter++
    res.send(`Visitas: ${counter}`)
})
app.get('/fyh', (req,res)=>{
    const fyh = new Date()
    res.send(`FyH: ${ fyh.getDate() }/${ fyh.getMonth() }/${ fyh.getFullYear() }  ${ fyh.getHours() }:${ fyh.getMinutes() }:${ fyh.getSeconds() }`)
})


const PORT = 4000
const server = app.listen(PORT || 8080, () => {
    console.log(`Servidor corriendo en el puerto: ${ server.address().port }`)
})
