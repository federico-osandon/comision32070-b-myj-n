const socket = io().connect()

const render = (data) => {
    const listadoProductos = document.querySelector('#products')
    const html = data.map(product =>    `<li>
                                            <strong>${product.name}</strong>
                                            <em>Precio: ${product.price}</em>
                                        </li>`)

    listadoProductos.innerHTML = html
    
}


socket.on('message-server', data => {
    render(data.products)
})

// function add product from form
const addProduct = (e) => {   
    const name = document.querySelector('#name').value
    const price = document.querySelector('#price').value
    const product = {
        name,
        price
    }
    // console.log(product)
    socket.emit('add-product', product)
    return false
   
}