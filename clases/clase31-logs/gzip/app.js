const express = require('express')
const compression = require('compression')

const app = express()

// app.use(compression())

const saludos = ('Hola que tal').repeat(1000)

app.get('/', (req, res) => {
    res.send('hello')
})

app.use('/saludo', (req, res)=>{
    res.send(saludos)
})
app.use('/saludozip', compression(),(req, res)=>{
    res.send(saludos)
    
})

app.listen(4000, err => {
    if(err) console.log(err)

    console.log('Escuchando el puerto 4000')
})

