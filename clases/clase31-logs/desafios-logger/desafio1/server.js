const express = require('express')
const { logger } = require('./logger')

const app = express()

app.get('/',( req, res ) => {
    res.send('hello world')
})

app.get('/suma',( req, res ) => {

    const { numUno, numDos } = req.query

    if (!numUno) {
        logger.error('numUno no esta definido')
        return res.send('el primer parametro tiene que ser válido')
    }
    
    if (!numDos) {
        logger.error('numDos no esta definido como parámetro')
        return res.send('el parámetro dos tiene que ser Válido')
    }
    
    logger.info('Los parámetros son correcto para la suma')
    res.send(`El resultado de ${numUno} + ${numDos} = ${parseInt(numUno) + parseInt(numDos)}`)
})

app.get('*', (req, res)=>{
    const { url, method } = req
    logger.warn('La ruta que trata de acceder no etá implementada.')
    res.send(`La ruta: ${url} - con el verbo: ${method},  no está implementada`)
})

const PORT = 4000

const server = app.listen(PORT, () => {
    logger.info(`Escuchando el puerto ${server.address().port}`)
})

server.on('Error', err => logger.error(`Error en el servidor: ${err}`))