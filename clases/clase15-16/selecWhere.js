const { options } = require('./mariaDB/conexionDB')

const knex = require('knex')(options)

knex.from('cars').select('*').where('price', '>=', 1000000).andWhere('p', '<=', 2000000)
.then(resp => console.log(resp))
.catch(err => console.log(err))
.finally(() => knex.destroy())