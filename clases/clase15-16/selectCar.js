const { options } = require('./mariaDB/conexionDB')

const knex = require('knex')(options)

knex.from('cars').select('*')
.then(resp => {
    for(obj of resp){
        console.log(`El id: ${obj.id} es un ${obj.name} y cuesta ${obj.price}`)
    }
})
.catch(err => console.log(err))