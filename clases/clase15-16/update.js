const { options } = require('./mariaDB/conexionDB')

const knex = require('knex')(options)


knex.from('cars').where('price', 100000).update({
    price: 1000000
})
.then(resp => console.log(resp))
.catch(err => console.log(err))
.finally(() => knex.destroy())
