const { options } = require('./mariaDB/conexionDB')

const knex = require('knex')(options)

const arrayCars =[
    {name: 'BMW', price: 100000},
    {name: 'Audi', price: 80000},
    {name: 'Mercedes', price: 120000},
    {name: 'Ferrari', price: 200000},
    {name: 'Porsche', price: 150000}
]

knex('cars').insert(arrayCars)
.then(resp => console.log(resp))
.catch(err => console.log(err))
.finally(() => knex.destroy())



knex.from('cars').select('*')
.then(resp => {
    for(obj of resp){
        console.log(`El id: ${obj.id} es un ${obj.name} y cuesta ${obj.price}`)
    }
})
.catch(err => console.log(err))