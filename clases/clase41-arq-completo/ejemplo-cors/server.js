const express = require('express')
const cors = require('cors')

const app = express()

// const corsOptions = {
//     origin: 'http://localhost:5173',
//     optionsSuccessStatus: 200,
//     methods: 'GET, PUT'
// }


// var allowList = ['http://localhost:5173', 'http://localhost:5174']

// const corsOptionsDelegate = (req, callback) => {
//     let corsOptions

//     let isDomainAllowed = allowList.indexOf(req.header('Origin')) !== -1
    
//     let isExtensionAllowed = req.path.endsWith('.jpg')
//     console.log(isExtensionAllowed)

//     if (isDomainAllowed) {
//         corsOptions = { origin: true }
//     }else {
//         corsOptions = { origin: false }
//     }
//     callback(null, corsOptions)
// }

// app.use(express.json())
// app.use(cors())
// app.use(cors(corsOptionsDelegate))

app.get('/api/productos',(req, res) => {
    res.json({
        message: 'Hello from productos'
    })
})
app.get('/api/', (req, res) => {
    res.json({
        message: 'Hello from server!'
    })
})

const port = process.env.PORT || 4000
app.listen(port, () => {
    console.log('Server is running on port '+port)
})


