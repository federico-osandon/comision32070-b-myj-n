import noticiaDTO from "../DTOs/noticias.DTOs";

const { default: NoticiasBaseDAO } = require("./noticiasBaseDao");

class NoticiasMemFileDAO extends NoticiasBaseDAO {
    constructor() {
        super()
        this.noticias = []
    }
    
    guardarNoticia = noticia => {
        try {
            let _id = this.getNext_Id(this.noticias)
            let fyh = new Date().toLocaleString()
            let noticiaGuardada = noticiaDTO(noticia, _id, fyh)
            this.noticias.push(noticiaGuardada)
            return noticiaGuardada
        } catch (error) {
            console.log('error guardarNoticia', error)
            let noticia = {}
            return noticia
        }
    }
    
    obtenerNoticias  = async _id => {
        try {
        if (_id) {
            let index =  this.noticias.findIndex((noticia) => noticia._id == _id);
            return index>= 0 ? [this.noticias[index]] : null
        } else {
            return this.noticias;
        }
        } catch (error) {
            console.log('error obtenerNoticias', error)
            return []
        }
    }
    
    async actualizarNoticia(_id, noticia) {
        try {
            let fyh = new Date().toLocaleString()
            let noticiaNew = noticiaDTO(noticia, _id, fyh)

            const index = this.getIndex(_id, this.noticias)
            let noticiaActual = this.noticias[index] || {}
            let noticiaActualizada = {...noticiaActual, ...noticiaNew}

            index >= 0 ?
                this.noticias.splice(index, 1, noticiaActualizada) :
                this.noticias.push(noticiaActualizada)

            return noticiaActualizada               
        } catch (error) {
            console.log('error actualizarNoticia', error)
            let noticia = {}
            return noticia
        }
    }
    
    async borrarNoticia(_id) {
        try {
            const index = this.getIndex(_id, this.noticias)
            let noticiaBorrada = this.noticias.splice(index,1)[0]
            return noticiaBorrada
        } catch (error) {
            console.log('error borrarNoticia', error)
            let noticia = {}
            return noticia
        }
    }
} 

export default NoticiasMemFileDAO