import fs from 'fs/promises'
import noticiaDTO from '../DTOs/noticias.DTOs'
import NoticiasBaseDAO from './noticiasBaseDao'


class NoticiasFileDAO extends NoticiasBaseDAO {
    constructor(nombreArchivo) {
        super()
        this.nombreArchivo = nombreArchivo
    }

    async leer(archivo) {        
        return JSON.parse(await fs.readFile(archivo, 'utf-8'))        
    }

    async guardar(archivo, noticias) {
        await fs.writeFile(archivo, JSON.stringify(noticias, null, '\t'))
    }

    obtenerNoticias = async _id => {
        try {
            let noticias = await this.leer(this.nombreArchivo)
            if (_id) {
                let index = noticias.findIndex(noticia => noticia._id === _id)

                return index >= 0 ? [noticias[index]] : []
            } else {
                return noticias
            }
        } catch (error) {
            console.log('error obtenerNoticias', error)
            let noticias = []
            
            await this.guardar(this.nombreArchivo, noticias)
            return noticias
        }
    }

    guardarNoticia = async noticia => {
        try {
            let noticias = await this.leer(this.nombreArchivo)

            let _id = this.getNext_Id(noticias)
            let fyh = new Date().toLocaleString()
            let noticiaGuardada = noticiaDTO(noticia, _id, fyh)
            noticias.push(noticiaGuardada)

            await this.guardar(this.nombreArchivo, noticias)
            return noticiaGuardada
        } catch (error) {
            console.log('error guardarNoticia', error)
            let noticia = {}
            return noticia
        }
    }

    actualizarNoticia = async (_id, noticia) => {
        try {
            let noticias = await this.leer(this.nombreArchivo)

            let fyh = new Date().toLocaleString()
            let noticiaNew = noticiaDTO(noticia, _id, fyh)

            const index = this.getIndex(_id, noticias)
            let noticiaActual = noticias[index] || {}

            let noticiaActualizada = {...noticiaActual, ...noticiaNew}

            index >= 0 ?
                noticias.splice(index, 1, noticiaActualizada) :
                noticias.push(noticiaActualizada)

            await this.guardar(this.nombreArchivo, noticias)

            return noticiaActualizada

        } catch (error) {
            console.log('error actualizarNoticia', error)
            let noticia = {}
            return noticia
        }
    }

    borrarNoticia = async _id => {
        try {
            let noticias = await this.leer(this.nombreArchivo)

            const index = this.getIndex(_id, noticias)
            let noticiaBorrada = noticias.splice(index, 1)[0]

            await this.guardar(this.nombreArchivo, noticias)

            return noticiaBorrada
        } catch (error) {
            console.log('error borrarNoticia', error)
            let noticia = {}

            return noticia
        }
    }
}

export default NoticiasFileDAO





