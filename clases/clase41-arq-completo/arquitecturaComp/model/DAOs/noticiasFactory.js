import NoticiasDBMongoDAO from "./noticiasDBMongo"
import NoticiasFileDAO from "./noticiasFileDAO"
import NoticiasMemFileDAO from "./noticiasMemDAO"



class NoticiasFactoryDAO {

    static get(tipo) {
        switch (tipo) {
            // case 'MEM':
            //     return new NoticiasMemFileDAO()
            // case 'FILE':
            //     return new NoticiasFileDAO(process.cwd()+'/noticias.json')
            case 'MONGO':
                return new NoticiasDBMongoDAO('miBase', 'noticias')
            // default:
            //     return new NoticiasMemFileDAO()
        }
    }
}   

export default NoticiasFactoryDAO