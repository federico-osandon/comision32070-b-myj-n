const { default: noticiaDTO } = require("../DTOs/noticias.DTOs");
const { default: NoticiasBaseDAO } = require("./noticiasBaseDao");
import mongodb from 'mongodb'
const { MongoClient, ObjectId } = mongodb

class NoticiasDBMongoDAO extends NoticiasBaseDAO {
    constructor(database, collection) {
        super()
    //    ;()()
        this.initConnection(database, collection)  // new NoticiasDBMongoDao()


        this.connection = null
    }

    

    initConnection = async (database, collection) => {
        console.log('Connecting to MongoDB...')

        this.connection = await MongoClient.connect('mongodb://localhost:27017', {
            useNewUrlParse: true,
            useUnifiedTopology: true
        })

        const db = connection.db(database)
        this.collection = db.collection(collection)

        console.log('Connected to MongoDB!')
    }

    obtenerNoticias = async _id => {
        try {
            if (_id) {
                console.log(_id)
                const noticia = await this.collection.findOne({ _id: ObjectId(_id) })
                console.log(noticia)
                return [noticia]
            } else {
                return await this.collection.find({}).toArray()
            }
        } catch (error) {
            console.log('error obtenerNoticias', error)            
        }
    }

    guardarNoticia = async noticia => {
        try {
            await this.collection.insertOne(noticia)
            return noticia
        } catch (error) {
            console.log('error guardarNoticia', error)
            return noticia
        }
    }

    actualizarNoticia = async (_id, noticia) => {
        try {
            await this.collection.updateOne({ _id: ObjectId(_id) }, { $set: noticia })
            return noticia
        } catch (error) {
            console.log('error actualizarNoticia', error)
            return noticia
        }
    }

    borrarNoticia = async _id => {
        let noticiaBorrada= noticiaDTO({}, _id, null)
        try {
            await this.collection.deleteOne({ _id: ObjectId(_id) })
            return noticiaBorrada
        } catch (error) {
            console.log('error borrarNoticia', error)
            return noticiaBorrada
        }
    }
}

export default NoticiasDBMongoDAO

