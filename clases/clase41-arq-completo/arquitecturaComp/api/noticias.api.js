import config from '../config'
import NoticiasFactoryDAO from '../model/DAOs/noticiasFactory.js'
import Noticias from '../model/models/noticias.models';
// antes metodos para persiste
// _________________________capa de negocio____________________//

class ApiNoticias {

    constructor() {
        this.noticiasDAO = NoticiasFactoryDAO.get(config.TIPO_PERSISTENCIA)// mono 
    }

    async obtenerNoticias(id) { 
        return await this.noticiasDAO.obtenerNoticias(id)
    }

    async guardarNoticias(noticia) { 
        ApiNoticias.asegurarNoticiaValida(noticia,true)
        return await this.noticiasDAO.guardarNoticia(noticia) 
    }

    static asegurarNoticiaValida(palabra,requerido) {
        try {
            Noticias.validar(palabra,requerido)
        } catch (error) {
            throw new Error('la Palabra posee un formato json invalido o faltan datos: ' + error.details[0].message)
        }
    }    

    async actualizarNoticia(id,noticia) { 
        ApiNoticias.asegurarNoticiaValida(noticia,false)
        return await this.noticiasDAO.actualizarNoticia(id,noticia) 
    }

    async borrarNoticia(id) { 
        return await this.noticiasDAO.borrarNoticia(id) 
    }
}

export default ApiNoticias