import { Router } from 'express';
import ControladorNoticias from '../controllers/noticias.controllers';

class RouterNoticias {
    constructor() {
        this.controladoreNoticias = new ControladorNoticias()
        this.router = Router()

    }

    start() {
        this.router.get('/:id', this.controladoreNoticias.obtenerNoticias)
        this.router.post('/', this.controladoreNoticias.guardarNoticia)
        this.router.put('/:id', this.controladoreNoticias.actualiazarNoticia)
        this.router.delete('/:id', this.controladoreNoticias.borrarNoticia)

        return this.router
    }
}

export default RouterNoticias