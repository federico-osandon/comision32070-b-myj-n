import { config } from './config'
import express from 'express'
const app = express()
import cors from 'cors'
import RouterNoticias from './routes/noticias.routes'
import { Server } from 'http'

if (config.NODE_ENV === 'development') {
    app.use(cors())
}

app.use(express.static('public'))
app.use(express.json())

const routerNoticias =  new RouterNoticias()

app.use('/api/noticias', routerNoticias.start())


const port = process.env.PORT || 8000

app.listen(port, () => {
    console.log('Server is running on port '+port)
})
Server.on('error', (error) => {
    console.log('Error en servidor: ', error)
})