const jwt = require('jsonwebtoken');
const { users } = require('../users/users');

function auth(req, res, next) {
    const authHeader = req.headers.authorization;
    PRIVATE_KEY = process.env.JWT_PRIVATE_KEY
    console.log(authHeader)
    if (!authHeader) {
      return res.status(401).json({
        error: 'not authenticated'
      });
    }
   
    // const token = authHeader.split(' ')[1];
    const token = authHeader
   
    jwt.verify(token, PRIVATE_KEY, (err, decoded) => {
        if (err) {
            return res.status(403).json({
                error: 'not authorized'
            });
        }        
        console.log(decoded.data)
        req.user = decoded.data;
        next()
    })
   }
   
    module.exports = { auth }