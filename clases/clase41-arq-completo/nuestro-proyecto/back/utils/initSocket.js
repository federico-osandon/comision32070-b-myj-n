const Mensajes = require("../services/mensajes")
const Products = require("../services/productos")

const products = new Products()
const messages = new Mensajes()

const  initSocket = (io) => {
    io.on('connection', async (socket) => {
        console.log('Nuevo cliente conectado!')

        /////////////////// productos ///////////////////////
        socket.emit('productos', products.getProducts())

        // socket.on('nuevoProducto', (data) => {
        //     console.log(data)
        //     products.addProduct(data)
        //     io.sockets.emit('productos', products.getProducts())
        // })
        // para la otra forma de enviar los datos
        socket.on('update', (data) => {
            console.log(data === 'ok')
            if (data === 'ok') io.sockets.emit('productos', products.getProducts())
        })
        
        
        /////////////////// mensajes ///////////////////////
        socket.emit('mensajes', await messages.getAll())

        socket.on('nuevoMensaje', async (data) => {
            console.log(data)
            await messages.save(data)
            io.sockets.emit('mensajes', await messages.getAll())
        })
    })
}

module.exports = initSocket