import Product from "../Product/Product"


const ProductsList = ({ products = []}) => {
  return (
    <center>
        { products.map(prod => <Product key={prod.id} prod={prod} /> ) }

    </center>
    
  )
}

export default ProductsList