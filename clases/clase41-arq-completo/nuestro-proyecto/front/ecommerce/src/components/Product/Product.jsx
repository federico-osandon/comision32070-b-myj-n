
const Product = ({prod}) => {
  return (
    <div 
            className="Noticias w-50" 
            style={{opacity: prod?.visita ? '0.5' : '1'}}
        >
            <div 
                className="media alert alert-primary my-4"
                style={{width: '350', borderRadius: 15}}
            >
                <img src={prod.imagen} alt={prod.titulo} />
                <div className="media-body ml-4">
                    {/* //___________________________________________________________________// */}
                    {/* //_________________               Leer             __________________ */}
                    {/* //___________________________________________________________________// */}
                    <button 
                        className="btn btn-outline-warning float-right"
                        onClick={()=>console.log('leída')}
                    > 
                        Leída 
                    </button>
                    {/* //___________________________________________________________________// */}
                    {/* //_________________               Borrar             __________________ */}
                    {/* //___________________________________________________________________// */}
                    <button 
                        className="btn btn-outline-danger float-left"
                        onClick={()=>console.log('borrar')}
                    > 
                        Borrar
                    </button>
                    {/* //___________________________________________________________________// */}
                    {/* //_________________           representación         __________________ */}
                    {/* //___________________________________________________________________// */}
                    <h3 className="text-center font-italic text-uppercase" ><u>Producto Nro {prod.id}</u></h3>
                    <br />
                    <h3>{prod.titulo}</h3>
                    <p><i>{prod.cuerpo}</i></p>
                    <p><b>{prod.autor}</b></p>
                    <p><b>{prod.email}</b></p>
                    <p><b>ID: {prod.id}</b></p>
                </div>
            </div>

        </div>
  )
}

export default Product