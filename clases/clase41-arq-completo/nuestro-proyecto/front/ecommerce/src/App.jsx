import { useState } from 'react'
import reactLogo from './assets/react.svg'
// import './App.css'
import ProductosListPage from './page/ProductosListPages/ProductosListPage'
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {  
  

  return (
    // <ProductosListPage /> -> ProductosListPage() en jsx
    <div className="App">
      <ProductosListPage />
    </div>
  )
}

export default App
