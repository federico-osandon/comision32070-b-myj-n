import axios from 'axios'
import { useState,useEffect } from 'react'
// import {  } from 'react'
import ProductsList from "../../components/ProductsList/ProductsList"

const ProductosListPage = () => {
    const [products, setProducts] = useState([])
    const [loading, setLoading] = useState(true)
    

    // setTimeout(async ()=>{
    const obtenerProductos = async () =>{
        try {
                let respuestaProducts = await axios('http://localhost:4000/api/productos')
                let {data}= respuestaProducts
                setProducts( data )

                
            } catch (error) {
                console.log(error)
            } finally {
                setLoading(false)
            }
        }
    // }, 5000)
    useEffect(() => {
        obtenerProductos()

    }, [])

    console.log(products)
    return (
        <div>
            {
                loading ? 
                    <h2>Cargando ...</h2>
                :
                    <ProductsList products={products}  />
            }
        </div>
    )
}

export default ProductosListPage