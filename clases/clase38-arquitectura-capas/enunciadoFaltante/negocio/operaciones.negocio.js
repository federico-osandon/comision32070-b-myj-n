const { guardarOperaciones, listarOperaciones } = require("../persistencia/operaciones.persistencia")



const sumar = async (mumero1, numero2) => {    
    return await guardarOperaciones({
        tipo: 'suma',
        params: [mumero1, numero2],
        resultado: mumero1 + numero2,
        fecha: new Date()
    })
} 

const listar = () => listarOperaciones()

module.exports = {
    sumar,
    listar
}
