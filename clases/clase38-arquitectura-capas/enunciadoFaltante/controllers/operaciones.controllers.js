const { sumaNeg, listarNeg } = require('../negocio/operaciones.negocio')
 
const sumaGet = (req, res) => {
    let { a, b } = req.query
    sumaNeg(Number(a), Number(b))
    res.send(`La suma de ${a} y ${b} se guardó en la base de datos`)
}

const restaGet = (req, res) => {
    let { a, b } = req.query
    res.send(`La resta de ${a} y ${b} es ${restar(Number(a), Number(b))}`)
}

const multGet = (req, res) => {
    let { a, b } = req.query
    res.send(`La multiplicación de ${a} y ${b} es ${mult(Number(a), Number(b))}`)
}

const divGet =  (req, res) => {
    let { a, b } = req.query
    res.send(`La división de ${a} y ${b} es ${div(Number(a), Number(b))}`)
}

const listarGet = (req, res) => {
    res.send(listarNeg())
}

module.exports = { sumaGet, restaGet, multGet, divGet, listarGet }