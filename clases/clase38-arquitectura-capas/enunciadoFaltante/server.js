const express = require('express')
const { sumar, restar, mult, div } = require('./helpers/operaciones')
const operacionesRouter = require('./routes/operaciones.routes')
// const { suma, resta, mult, div } = require('cds-operaciones')


const app = express()

app.use('/api/operaciones', operacionesRouter)

const PORT = 8080
const server = app.listen(PORT, () => {
    console.log(`Servidor express escuchando en el puerto ${server.address().port}`)
})
server.on('error', error => console.error(`Error en servidor`, error))