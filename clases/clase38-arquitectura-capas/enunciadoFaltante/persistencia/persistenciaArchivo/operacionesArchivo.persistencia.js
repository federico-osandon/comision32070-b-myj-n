const fs = require('fs')

const guardarOperaciones = async (objNegocio) => {
    // operaciones.push(objNegocio)
    try {
        await fs.promises.writeFile('operaciones.txt', JSON.stringify(objNegocio))        
    } catch (error) {
            console.log(error)       
    }
}

const listarOperaciones = async () => {
    try {         
        return await fs.promises.readFile('operaciones.txt', 'utf-8')        
    } catch (error) {
        return []
    }
}

module.exports = {
    guardarOperaciones,
    listarOperaciones
}