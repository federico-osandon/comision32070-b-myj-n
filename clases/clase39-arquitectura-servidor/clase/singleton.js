// ejemplo de clase con singleton
let instance = null

class SingletonClass {
    constructor() {
        this.value = Math.random(100)
    }

    printValue() {
        console.log(this.value)
    }

    static getInstance() {
        if (!instance) {
            instance = new SingletonClass()
        }
        return instance
    }
}

const obj1 = SingletonClass.getInstance()
const obj2 = SingletonClass.getInstance()

obj1.printValue()
obj2.printValue()

console.log(obj1 === obj2)