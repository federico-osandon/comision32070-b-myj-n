// lib/db.js
const level = require('level');
const sublevel = require('level-sublevel');

module.exports = function (dbName) {
    return sublevel(
        level(dbName, { valueEncoding: 'json' })
    );
}

//_____________________________________________________________________________________________/

// lib/userService.js
module.exports = function (db) {
    const users = db.sublevel('users');
    
    return {
        findUsers: function () {
            return users.get();
        },
        saveUser: function (user) {
            return users.save(user);
        }
    };
}
//_____________________________________________________________________________________________/

// lib/userController.js
module.exports = function (userService) {
    return {
        findUsers: function (req, res, next) {
            userService.findUsers()
                .then(findUsersOk)
                .catch(handleError);
        },
        saveUser: function (req, res, next) {
            userService.saveUser(req.body)
                .then(saveUserOk)
                .catch(handleError);
        }
    };
}
//_____________________________________________________________________________________________/

// app.js
const express = require('express');
const app = express();
const bodyParser = require('bodyParser');

const db = require('./lib/db')('example-db');
const userService = require('./lib/userService')(db);
const userController = require('./lib/userController')(userService);

app.use(bodyParser.json());

app.get('/users', userController.findUsers);
app.post('/users', userController.saveUser);

app.listen(3000, function () {
    console.log('Servidor funcionando correctamente');
});
//_____________________________________________________________________________________________/
//_____________________________________________________________________________________________/