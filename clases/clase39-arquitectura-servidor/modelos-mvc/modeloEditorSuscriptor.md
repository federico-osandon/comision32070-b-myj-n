El modelo Suscriptor/Editor es un patrón de arquitectura de software que se utiliza para 
implementar la comunicación entre objetos. En este patrón, los objetos suscriptores se suscriben 
a un objeto editor para recibir notificaciones cuando cambia el estado del objeto editor. 
El objeto editor mantiene una lista de suscriptores y notifica a cada uno de ellos cuando 
ocurre un cambio en su estado. Este patrón es útil en aplicaciones en las que varios objetos deben ser notificados de un cambio en otro objeto.


ej: 

Un ejemplo común en el que se utiliza el patrón Suscriptor/Editor es en una aplicación de redes 
sociales. Cuando un usuario publica un nuevo mensaje en su perfil, todos los usuarios que lo 
siguen deben ser notificados de ese nuevo mensaje. En este caso, el usuario que publica el 
mensaje es el editor y los usuarios que lo siguen son los suscriptores. 
El usuario que publica el mensaje mantiene una lista de suscriptores y notifica a cada uno de 
ellos cuando publica un nuevo mensaje. Otro ejemplo podría ser en una aplicación de trading 
financiero, donde un usuario puede suscribirse a una acción y ser notificado cuando el precio de 
la acción cambie.