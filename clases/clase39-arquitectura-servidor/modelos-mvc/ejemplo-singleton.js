let instance = null


class SingletonClass{
    constructor(){
        this.value = Math.random(100)
    }

    printValue(){
        console.log(this.value)
    }

    static getInstance(){
        if(!instance){
            instance = new SingletonClass()
        }
        return instance
    }
} 

const instance1 = SingletonClass.getInstance()
const instance2 = SingletonClass.getInstance()

instance1.printValue()
instance2.printValue()

console.log(instance1 === instance2) // true