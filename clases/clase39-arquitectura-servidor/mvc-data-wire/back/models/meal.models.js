const mongoose = require('mongoose')

const mealModel = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, required: true },
    price: { type: Number, required: true },
    type: { type: String, required: true }
})


module.exports = mongoose.model('Meal', mealModel)