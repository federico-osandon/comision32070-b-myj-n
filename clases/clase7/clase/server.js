const express = require('express')


const app = express()

app.use(express.json())
app.use(express.urlencoded({extended: true}))

// app.get('/api/usuarios', (req, res) => {
//     res.status(200).json([
//         { id: 1, nombre: 'Juan' },
//         { id: 2, nombre: 'Pedro' },
//         { id: 3, nombre: 'Maria' }
//     ])
// })

// app.get('/api/mensajes', (req, res) => {
//     // console.log(req)
//     const { nombre, pass } = req.query
//     console.log(Object.entries(req.query))
//     console.log(Object.entries(req.query).length)

//     if (Object.entries(req.query).length > 0) {
//         res.status(200).json({
//             result: 'Get query params: ok.',
//             query: req.query
//         })
//     }else {
//         res.status(200).json({            
//             result: 'get all: ok'
//         })
//     }    
// })

// app.get('/api/mensajes/:id', (req, res) => {
    
//     const { id } = req.params
//     // console.log(id)
//     res.json({
//         mensaje: 'ok', 
//         id
//     })
       
// })

// app.post('/api/mensajes', (req, res)=> {
//     console.log(req.body)
//     const { nombre } = req.body
//     res.json({status: 'ok', nombre})
// })


// app.put('/api/mensajes/:id', (req, res)=>{
//     console.log(req.params)
//     console.log(req.body)

//     const { id } = req.params
//     const { nombre } = req.body
//     res.json({
//         status: 'ok',
//         id,
//         nombre
//     })
// })


app.delete('/api/mensajes/:id', (req, res)=>{
    console.log(req.params)
    const { id } = req.params
    res.json({
        status: 'ok',
        mensajes: 'delete',
        id
    })
})

/* This is a way to set the port for the server. The first part is setting the port to the environment
port or 4000. The second part is setting the server to listen on the port. */
const PORT = process.env.PORT || 4000
const server = app.listen(PORT, () => {
    console.log(`Server running on port ${server.address().port}`)
})