const express = require('express')

const app = express()



const nombres = ['Luis', 'Lucía', 'Juan', 'Augusto', 'Ana']
const apellidos = ['Pieres', 'Cacurri', 'Bezzola', 'Alberca', 'Mei']
const colores = ['rojo', 'verde', 'azul', 'amarillo', 'magenta']
let arrayPersona = []

const getElementRandom = (array) => {
    return array[Math.floor(Math.random() * array.length)]
}

const createArrayPersona = () => {    
    return {
            nombre: getElementRandom(nombres),
            apellido: getElementRandom(apellidos),
            color: getElementRandom(colores),
    }
}


app.get('/', (req, res) => {
    
    res.send('Hola Mundo')
})


app.get('/test', (req, res) => {
    arrayPersona.push(createArrayPersona())    
    res.send(arrayPersona)
})

// listen port 4000
app.listen(4000, () => {
    console.log('Server on port 4000')
})