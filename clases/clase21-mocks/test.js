const assert = require('assert')

class Calculadora {
    static sumar(numero1, numero2) {
        return numero1 + numero2
    }
}

const testSum = () => assert.equal(8, Calculadora.sumar(3, 5))

testSum()

