import { useEffect, useState } from 'react'
import ItemList from '../../ItemList/ItemList'

// const options = {
    //     method: 'GET',
    //     headers: {
        //         'Content-Type': 'application/json'{
            //         ' token': localStorage.getItem('token'),
            // 
            //     },
            //     body: JSON.stringify({
            //         username: 'test',
            //         password: 'test'
            // }

const ItemListPage = () => {
    const [products, setProducts] = useState([])
    const [loading, setLoading] = useState(true)

    useEffect(()=>{
        fetch('http://localhost:4000/api/productos')
        .then(res => res.json())      
        .then(data => setProducts(data))  
        .catch(err => console.log(err))
        .finally(() => setLoading(false))
    },[])
    console.log(products)
    return (
        <>
            {
                loading ? <h1>Cargando...</h1> : <ItemList products={products} />
            }
        </>
    )
}

export default ItemListPage