import {fork} from 'child_process'

const forked = fork('./cihld.js')

forked.on('message', (message) => {
    if (message === 'listo') {
        forked.send('Hola¡¡¡')
    } else {
        console.log('Mensaje del proceso hijo: ', message)
        
    }
})

// console.log('Comienzo del Proograma Padre')
