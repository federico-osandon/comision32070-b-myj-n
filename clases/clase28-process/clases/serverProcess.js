// const  dotenv = require('dotenv').config()
// const process = require('process')

// console.log(process.cwd()) 
// console.log(process.pid) 
// console.log(process.ppid) 
// console.log(process.version)
// console.log(process.title)
// console.log(process.platform)
// console.log(process.memoryUsage())
// console.log(process)

////////////////////// exit ///////////////////////////

// setInterval(() => {
//     console.log('Hola')
// }, 2000)


// setTimeout(() => {
//     process.exit(3)
// }, 5000)

////////////////////////// on ///////////////////////////
    

    // process.on('beforeExit', (code) => {
    //         console.log('El proceso va a terminar', code) // 1 orden aparente
    // })
    
    // process.on('exit', (code) => { // 2 orden aparente
    //     console.log('Exit', code)
    // })

    // console.log('This message is displayed first.') //3 orden aparente


///////////////////////////////// uncaughtException ///////////////////////////

// process.on('uncaughtException', (err, origin) => {
//     console.error('We forgot to catch an error', err)
// })

// setTimeout(() => {
//     console.log('This will still run.')
// }, 500)

// // Se fuerza una excepción pero no se recoge
// nonexistentFunc()
// console.log('Esto no se ejecutará')


/////////////////////// process execPath ///////////////////////

// console.log(process.execPath) // /usr/local/bin/node

/////////////////////// process stdout write ///////////////////////

// console.log  = (message) => {
//     process.stdout.write(` - ${message} \n`)
// }
// console.log('hola')
// console.log('Fede')
// console.log('Cómo estás?')