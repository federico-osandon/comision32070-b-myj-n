const {fork} = require('child_process')

const forked = fork('./cihld.js')

forked.on('message', (message) => {
    console.log('Mensaje del proceso hijo: ', message)
})

console.log('Comienzo del Proograma Padre')

setTimeout(() => {
    forked.send('Hola¡¡¡')
}, 2000)