import { Router } from '../deps.ts'

import {
    createUser,
    deleteUser,
    findUser,
    updateUser
} from '../handlers/user.ts'

export const routerUser = new Router()
// User routes
.get('/api/users', (ctx) => {
    ctx.response.body = 'Bienvenido a esta api'
})
.get('/api/users/:userid', findUser)
.delete('/api/users/:userId', deleteUser)
.patch('/api/users', updateUser)
.post('/api/users', createUser)