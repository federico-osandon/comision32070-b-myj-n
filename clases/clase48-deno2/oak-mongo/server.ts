import { Application, config } from "./deps.ts";

import { logger } from "./logger/logger.ts";
import { routerUser } from "./routes/user.routes.ts";

// rutas nuevas ______________________________________________________________________________

import routerQuote from './routes/quote.routes.ts'

//_______________________________________________________________________________________________
const configData = await config()
const PORT = configData['PORT']

const app = new Application();

app.use(logger)
app.use(routerUser.routes())

// nuevos___________________________________________________________________________________________

app.use(routerQuote.routes())
app.use(routerQuote.allowedMethods())

// nuevos___________________________________________________________________________________________

console.log(`Escuchando en el puerto ${PORT}`)

await app.listen({ port: Number(PORT) })