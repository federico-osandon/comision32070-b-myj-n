import { Application } from "./deps.ts";


const app = new Application();

/* `use` is a middleware function that takes a callback function. */
app.use( context => {
    context.response.body = "Hello World!";
});


console.log(`Corriendo en el puerto 8000`)
await app.listen({ port: 8000 });