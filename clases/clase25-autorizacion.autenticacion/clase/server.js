const express = require('express')
const session = require('express-session')
const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')
const ejs = require('ejs')

const Users = require('./users')

const app = express()

// app.use(express.static('public'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')

////////////////////// utils //////////////////////

const isValidPassport = (user, password) => {
    return bcrypt.compareSync(password, user.password)
}

const createHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null)
}

///////////////////////// midleware passport /////////////////////////
passport.use('login' ,new LocalStrategy(
    (username,password, done) => {
        const user = Users.find(usuario => usuario.username === username)

        if(!user) {
            console.log(`Usuario ${username} no encontrado`)
            return done(null, false, {message: 'Usuario no encontrado'})
        }
        // Validación de contraseña
        if(user.password !== password) {
            console.log(`Contraseña incorrecta`)
            return done(null, false, {message: 'Contraseña incorrecta'})
        }
        return done(null, user)
    })
)


passport.use('signup', new LocalStrategy({
    passReqToCallback: true
},
    (req, username, password, done) => {
        const user = Users.find(usuario => usuario.username === username)

        if(!user) {
            console.log(`Usuario ${username} ya existe`)
            return done(null, false, {message: 'Usuario no encontrado'})
        }
        const {name, email} = req.body
        
        const newUser = {
            id: Users.length + 1,
            name,
            email,
            password,
            username
        }
        
        Users.push(newUser)

        return done(null, userWithId)
    }
))

///// serializar y deserializar usuario /////
passport.serializeUser((user, done) => {
    done(null, user.id)
})

passport.deserializeUser((id, done) => {
    const user = Users.find(usuario => usuario.id === id)
    done(null, user)
})

///////////////////////// midleware express /////////////////////////



app.use(session({
    secret: 'secreto',
    cookie: {
        httpOnly: false,
        secure: false,
        maxAge: 1000 * 60 * 60 * 24
    },
    rolling: true,
    resave: true,
    saveUninitialized: false
}))

app.use(passport.initialize())
app.use(passport.session())


// index
app.get('/home', (req, res) => {
    res.render('profileUser',{user: req.user})
})

// login 

app.get('/login', (req, res) =>{
    // if (req.isAuthenticated) {
    //     let user = req.user
    //     console.log('usuario logueado')
    //     res.render('profileUser', {user})
    // } else {
    //     console.log('userario no logueado')
    //     res.render('login')
    // }
    res.render('login')
})

app.post('/login', passport.authenticate('login',{
    successRedirect: '/home',
    failureRedirect: '/login'
}))

// signup

// logout
app.get('/logout', (req, res, next) => {
    req.logout((err) => {
        if (err) { return next(err) }
        res.redirect('/login')
    })
})
// fail route

// listen on port 4000 sfd
const port = 4000
const server = app.listen(port, () => {
    console.log(`Escuchando en el puerto ${server.address().port}`)
})
