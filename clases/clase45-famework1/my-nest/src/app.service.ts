import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}
// nest generate module cats
// nest generate controller cats
// nest generate service cats
