const express = require('express')
const fs = require('fs')

const app = express()

app.engine('ntl', (filePath, options, callback)=>{
    fs.readFile(filePath, (err, archivo)=>{
        if(err){
            return callback(new Error(err))
        }
        const rendered = archivo
                            .toString()
                            .replace('#title#', ''+options.title+'')
                            .replace('#message#', ''+options.message+'')

        return callback(null, rendered)
    })
})

app.set('views', './views')
app.set('view engine', 'ntl')

app.get('/' , (req, res)=>{
    res.render('index', {
        title: 'Renderizado con NTL',
        message: 'ASí se renderiza con NTL'
    })
})

app.listen(4000, ()=>{
    console.log('Servidor corriendo en el puerto: 4000')
})
