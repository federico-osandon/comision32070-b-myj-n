const express = require('express')

const app = express()

app.use(express.static('public'))

const server = app.listen(4000, () => {
    console.log( `Servidor corriendo en el puerto: ${server.address().port}` )
})