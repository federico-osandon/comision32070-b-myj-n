const { response } = require('express')
const express = require('express')
const handlebars = require('express-handlebars')

const app = express()

app.engine(
    'hbs',
    handlebars.engine({
        extname: 'hbs',
        defaultLayout: 'index.hbs',
        layoutsDir: __dirname + '/views/layouts',
        partialsDir: __dirname + '/views/partials'
    })
)

app.set('view engine', 'hbs')
app.set('views', './views')

app.get('/', (req, res) => {
    res.render('main', {isRender: true, users:  [
        {nombre: 'Juan', edad: '20'},
        {nombre: 'Pedro', edad: '30'},
        {nombre: 'Maria', edad: '40'}        
    ]})
})

app.listen(4000, err => {
    if (err) {
        throw new Error(`Error en el servidor: ${err}`)
    }
    console.log('Server running on port 4000')
})