const express = require('express')


const app = express()

// app.use(express.json())
// app.use(express.urlencoded({extended: false}))

app.set('views', './views')
app.set('view engine', 'pug')

app.get('/hola', (req, res) => {
    res.render('hello', {mensaje: 'Usando pug en express, este es el mensaje'})
})
app.get('/datos', (req, res) => {
    const { min,value, max, titulo } =  req.query
    // console.log(obj)
    res.render('hello1', { min, value, max, titulo })
})

app.listen(4000, err => {
    if (err) throw new Error(`Error en el servidor: ${err}`)

    console.log('Server running on port 4000')
})
