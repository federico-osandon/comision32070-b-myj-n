// crear app
const express = require('express')
const app = express()

app.use( express.urlencoded({extended: true}) )
// configurar puerto
const port = process.env.PORT || 4000

app.set('view engine', 'ejs')
app.set('views', './views')


let productos = [
    
]

//configurar get 
app.get('/', (req, res) => {


    res.render('pages/index',{
        mensaje: 'Hola ejs',
        productos
    })
} )

app.post('/productos', (req, res) => {
    const obj = req.body
    console.log(obj)
    
    productos.push(obj)
    res.render('pages/index',{
        mensaje: 'Hola ejs',
        productos
    })

})



// configurar en que puerto escucha
app.listen(port, err => {
    if (err) throw new Error(`Error on server listen: ${err}`)
    console.log(`Server running on port ${port}`)
})
