const express = require('express')
const connectionDB = require('./mongoDB/connection')
const Usuarios = require('./models/usuarios.model')

const app = express()

connectionDB()

app.use('/api/inicio', async (req, res)=>{
    let usuario = new Usuarios({
        nombre: 'Juan',
        apellido: 'Perez',
        dni: 12345678,
        email:'f@gmial.com'
    })
    console.log(usuario)
    await usuario.save()
    res.send('Hola mundo')
})

// listen port 4000
app.listen(4000, (err)=>{
    if(err) return console.log('Error al iniciar el servidor', err)
    console.log('Server on port 4000')
})