// docs - https://vintage-kitchen.com/es/faq/what-is-usenewurlparser-in-mongoose/ 
const mongoose = require('mongoose')

const connectionDB = async () => {
    try{
        let url = 'mongodb://localhost:27017/ecommerce'
        await mongoose.connect(url, {
            useNewUrlParser: true, //para que los usuarios puedan usar el analizador anterior si encuentran un error en el analizador nuevo.
            useUnifiedTopology: true // const client = new MongoClient ('mongodb: // call:[email protected]: 27017 /? ReplicaSet = rs'); col const = cliente. db ("prueba").
        })
        console.log('Database connected')
    }catch(err){
        console.log(err)
    }
    
    
}

module.exports = connectionDB  