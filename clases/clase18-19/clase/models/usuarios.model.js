const mongoose = require('mongoose')

const UsuariosSchema = mongoose.Schema({
    nombre: {
        type: String,
        max: 50,
        trim: true,
        required: true       
    },
    apellido: {
        type: String,
        max: 50,
        trim: true,
        required: true       
    },
    dni: {
        type: Number,
        required: true
    },
    email: {
        type: String,
        unique: true,
        trim: true,
        max: 50,
        unique: true,
        required: true
    }
})

module.exports = mongoose.model('Usuarios', UsuariosSchema)