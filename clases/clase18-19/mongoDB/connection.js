const mongoose = require('mongoose')


// mongodb+srv://federicoosandon:Federico1@cluster0.r3sreep.mongodb.net/ -> compass
// mongodb+srv://federicoosandon:Federico1@cluster0.r3sreep.mongodb.net/?retryWrites=true&w=majority -> códig




const connectDB = async () => {
    try {
        // const url = 'mongodb://localhost:27017/ecommerce'
        // process.env.MONGODB_CONNECT
        const url = process.env.MONGODB_CONNECT
        await mongoose.connect(url,{
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        console.log('MongoDB connected')
    } catch (error) {
        console.error(error)
    }
}

module.exports = connectDB