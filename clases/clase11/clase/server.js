const express = require('express')
const { Server: ServerHttp } = require('http')
const { Server: ServerIo } = require('socket.io')

const app = express()
const serverHttp = new ServerHttp(app)
const io = new ServerIo(serverHttp)

app.use(express.static('public'))

app.get('/', (req, res) => {
    console.log(__dirname)
    res.sendFile('index.html', { root: __dirname })
})

io.on('connection', socket => {
    console.log('New user connected')
    socket.emit('mensaje', 'eSte es mi mensaje desde el servidor')
    // socket.on('disconnect', () => {
    //     console.log('User disconnected')
    // })
    socket.on('respuesta', data => {
        console.log(data)
    })
})

serverHttp.listen(4000, () => {
    console.log('Escuchando el puerto 4000')
})