const socket = io.connect()

let input = document.querySelector('input')
let button = document.querySelector('button')

button.addEventListener('click', ()=>{
        socket.emit('mj-cliente', input.value)            
    }
)

socket.on('mensaje-server', data => {
    console.log(data)
    let dataMensaje = data.map(men => `<li>${men.mensaje}</li>`)  
    document.querySelector('#lista').innerHTML = dataMensaje
})