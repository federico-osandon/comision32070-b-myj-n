let productosDao

switch ('txt') {
    case 'txt':
        const { default: ProductosDaoArchivo } = await import('../daos/ProductosDaoArchivo.js')        
        productosDao = new ProductosDaoArchivo('../archivos/productos.txt')

        break;

    default:
        break;
}

module.exports = { productosDao }   